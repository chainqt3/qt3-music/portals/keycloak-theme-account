<#macro mainLayout active bodyClass>
    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="robots" content="noindex, nofollow">

        <title>${msg("accountManagementTitle")}</title>
        <link rel="icon" href="${url.resourcesPath}/img/favicon.ico">
        <#if properties.stylesCommon?has_content>
            <#list properties.stylesCommon?split(' ') as style>
                <link href="${url.resourcesCommonPath}/${style}" rel="stylesheet"/>
            </#list>
        </#if>
        <#if properties.styles?has_content>
            <#list properties.styles?split(' ') as style>
                <link href="${url.resourcesPath}/${style}" rel="stylesheet"/>
            </#list>
        </#if>
        <#if properties.scripts?has_content>
            <#list properties.scripts?split(' ') as script>
                <script type="text/javascript" src="${url.resourcesPath}/${script}"></script>
            </#list>
        </#if>
    </head>
    <body class="admin-console user ${bodyClass}">

    <#--    <header class="navbar navbar-default navbar-pf navbar-main header">-->
    <#--    <header>-->
    <#--        <div style="display: flex; flex-direction: row; width: 100%; border: solid red">-->
    <#--            <div style="width: 100%">-->
    <#--                <a href="http://localhost:3000/" style="border: solid">-->
    <#--                    <img class="navbar-title" style="width: 100%; height: 100px;">-->
    <#--                </a>-->
    <#--            </div>-->
    <#--            <div style="width: 100%">-->
    <#--                <p>test</p>-->
    <#--            </div>-->
    <#--        </div>-->
    <#--    </header>-->
    <#--    <nav class="navbar" role="navigation">-->
    <#--        <div class="navbar-header" style="width: 100%">-->
    <#--                <div style="border: solid">-->

    <#--                    <a href="http://localhost:3000/"style="border: solid">-->
    <#--                        <img class="navbar-title" style="width: 100%; height: 100px;">-->
    <#--                    </a>-->
    <#--                </div>-->

    <#--                <div class="container">-->
    <#--                </div>-->
    <#--        </div>-->
    <#--            <div class="navbar-collapse navbar-collapse-1">-->
    <#--                <div class="container">-->
    <#--                    <ul class="nav navbar-nav navbar-utility">-->
    <#--                        <#if realm.internationalizationEnabled>-->
    <#--                        <li>-->
    <#--                            <div class="kc-dropdown" id="kc-locale-dropdown">-->
    <#--                                <a href="#" id="kc-current-locale-link">${locale.current}</a>-->
    <#--                                <ul>-->
    <#--                                    <#list locale.supported as l>-->
    <#--                                        <li class="kc-dropdown-item"><a href="${l.url}">${l.label}</a></li>-->
    <#--                                    </#list>-->
    <#--                                </ul>-->
    <#--                            </div>-->
    <#--                        <li>-->
    <#--                                </#if>-->
    <#--                                <#if referrer?has_content && referrer.url?has_content>-->
    <#--                            <li><a href="${referrer.url}" id="referrer">${msg("backTo",referrer.name)}</a></li></#if>-->
    <#--                            <li><a href="${url.getLogoutUrl()}">${msg("doSignOut")}</a></li>-->
    <#--                        </ul>-->
    <#--                </div>-->
    <#--        </div>-->
    <#--    </nav>-->
    <#--    </header>-->
    <#--    <div style="display:flex; justify-content: end; border: solid">-->
    <#--        <div class="form-group">-->
    <#--            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">-->
    <#--                <div class="">-->
    <#--                    <a href="${url.getLogoutUrl()}">-->
    <#--                        <button type="submit"-->
    <#--                                class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}"-->
    <#--                                name="submitAction" value="Save">-->
    <#--                            ${msg("doSignOut")}-->
    <#--                        </button>-->
    <#--                    </a>-->
    <#--                </div>-->
    <#--            </div>-->
    <#--        </div>-->
    <#--    </div>-->

    <div class="headerMenu">
        <div style="width: 100px">
            <a href="http://localhost:3000/" style="">
                <div style="display: flex; justify-content: center; margin-top: 20px; margin-bottom: 20px;">
                    <img src="https://s3.eu-central-1.amazonaws.com/lambda.education.storage/lambda/education.jpg"
                         alt="logo" class="logoImg" style="width: 50px; height: 50px;">
                </div>
            </a>
        </div>
        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <a href="${url.getLogoutUrl()}">
                        <button type="submit"
                                class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}"
                                name="submitAction" value="Save">
                            ${msg("doSignOut")}
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="sideMenuContainer">
        <div class="account-bs-sidebar col-xs-6 col-sm-2 ">
            <p class="sidemenu-title">Settings</p>
            <ul>
                <li class="<#if active=='account'>active</#if>"><a href="${url.accountUrl}">${msg("account")}</a></li>
                <#if features.passwordUpdateSupported>
                <li class="<#if active=='password'>active</#if>"><a href="${url.passwordUrl}">${msg("password")}</a>
                    </li></#if>
                <li class="<#if active=='totp'>active</#if>"><a href="${url.totpUrl}">${msg("authenticator")}</a></li>
                <#if features.identityFederation>
                <li class="<#if active=='social'>active</#if>"><a
                        href="${url.socialUrl}">${msg("federatedIdentity")}</a></li></#if>
                <li class="<#if active=='sessions'>active</#if>"><a href="${url.sessionsUrl}">${msg("sessions")}</a>
                </li>
                <#--                <li class="<#if active=='applications'>active</#if>"><a-->
                <#--                        href="${url.applicationsUrl}">${msg("applications")}</a></li>-->
<#--                <#if features.log>-->
<#--                <li class="<#if active=='log'>active</#if>"><a href="${url.logUrl}">${msg("log")}</a></li>-->
<#--                </#if>-->
<#--                <#if realm.userManagedAccessAllowed && features.authorization>-->
<#--                <li class="<#if active=='authorization'>active</#if>"><a href="${url.resourceUrl}">${msg("myResources")}</a></li>-->
<#--                </#if>-->
            </ul>
        </div>
        <div class=" col-xs-10 content-area">
            <#if message?has_content>
                <div class="alert alert-${message.type}">
                    <#if message.type=='success' ><span class="pficon pficon-ok"></span></#if>
                    <#if message.type=='error' ><span class="pficon pficon-error-circle-o"></span></#if>
                    <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                </div>
            </#if>

            <#nested "content">
        </div>
    </div>

<#--    <script language="javascript">-->
<#--        setTimeout(function () {-->
<#--            window.location.reload();-->
<#--        }, 1000);-->
<#--    </script>-->

    </body>
    </html>
</#macro>
